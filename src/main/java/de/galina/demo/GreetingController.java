package de.galina.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    @GetMapping("/greeting")
    public Greeting greeting(){
        var g=new Greeting();
        g.setMessage("Hallo, wie geht's?");
        g.setSender("Andi");
        return g;
    }
}
